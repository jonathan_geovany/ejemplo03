package com.ejemplo03.controller;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import com.ejemplo03.model.Product;

public class ProductController {
	private ArrayList<Product> products;
	private final String DELIMITER = "|";

	/**
	 * Constructor que inicializa el controlador
	 */
	public ProductController() {
		super();
		products = new ArrayList<>();
	}
	
	/**
	 * agrega un producto a la lista unicamente si contiene id,nomnbre y precio
	 * @param p
	 * @return
	 */
	public boolean addProduct(Product p) {
		if(!p.getId().isEmpty() && !p.getName().isEmpty() && p.getPrice()!=0) {
			products.add(p);
			return true;
		}
		return false;
	}

	public ArrayList<Product> getProducts() {
		return products;
	}
	/**
	 * elimina un producto basado en el id unico
	 * @param id
	 * @return true si se elimino, false si no se encontro el producto
	 */
	public boolean deleteProduct(String id) {
		int index = indexOfProductById(id);
		if(index>=0) {
			products.remove(index);
			return true;
		}
		return false;
	}
	
	/**
	 * Actualiza el producto y evalua si este existe o no dentro de la lista
	 * @param p producto a ser actualizado
	 * @return true si se encontro y actualizo el producto
	 */
	public boolean updateProduct(Product p) {
		int index = indexOfProductById(p.getId());
		if(index>=0) {
			products.set(index, p);
			return true;
		}
		return false;
	}
	
	/**
	 * Busca el producto dentro de la lista basado en la propiedad id
	 * @param id unico para cada elemento de la lista
	 * @return el producto que se guarda en la lista
	 */
	public Product findProductById(String id) {
		for (Product product : products) {
			if (product.getId().equals(id)) {
				return product;
			}
		}
		return null;
	}
	
	/**
	 * busca un producto basado en la propiedad nombre
	 * @param name comparacion sin tomar en cuenta mayusculas o minusculas
	 * @return el producto encontrado con el nombre, null si no se encontro
	 */
	public Product findProductByName(String name) {
		for (Product product : products) {
			if (product.getName().equalsIgnoreCase(name)) {
				return product;
			}
		}
		return null;
	}
	/**
	 * 
	 * Busca la posicion de un producto basado en el id
	 * @param id unico para cada elemento de la lista
	 * @return entero que es la posicion en la lista, -1 si no encontro el elemento
	 */
	public int indexOfProductById(String id) {
		Product result = findProductById(id);
		if (result!=null) {
			return products.indexOf(result);
		}
		return -1;
	}
	
	/**
	 * Guarda datos en un archivo de texto en la ruta especificada
	 * @param path
	 * @return true si se pudo guardar, false si no se guarda 
	 */
	public boolean saveProducts(String path) {
		if(products.size()>0) {
			BufferedWriter writer;
			try {
				writer = new BufferedWriter(new FileWriter(path));
				for (Product product : products) {
					String line = product.getId()+DELIMITER+product.getName()+DELIMITER+product.getPrice()+DELIMITER+product.getDateFormated();
					writer.write(line);
					writer.newLine();
				}
				writer.close();
				return true;
			} catch (IOException e) {
				return false;
			}
		}else {
			System.out.println("No hay datos que guardar");
			return false;
		}
		
	}
	
	/**
	 * Carga productos almacenados en un archivo local
	 * @param path ruta al archivo
	 * @return true si se cargo correctamente los archivos, false si se obtuvo un error
	 */
	public boolean loadProducts(String path) {
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(path));
			products.clear();
			String line = reader.readLine();
			if(line==null) {
				System.out.println("No hay datos que cargar");
				reader.close();
				return false;
			}
			while (line!=null) {
				Product product  = parseProduct(line);
				if(product!=null) {
					products.add(product);
				}
				line = reader.readLine();
			}
			reader.close();
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * convierte elementos separados por el token : en datos para el producto
	 * @param data cadena de elementos separados por :
	 * @return Producto que se obtiene de la cadena de texto
	 */
	private Product parseProduct(String data) {
		String elements[] = data.split(DELIMITER);
		Product p = new Product();
		try {
			p.setId(elements[0]);
			p.setName(elements[1]);
			p.setPrice( Float.parseFloat( elements[2]));
			p.setDate_purchase(elements[3]);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Error en la lectura del producto");
		}
		return p;
	}
	

}
