package com.ejemplo03.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Product {
	private String id;
	private String name;
	private float price;
	private Date date_purchase;
	private SimpleDateFormat simpleDateFormat;
	
	public Product() {
		super();
		simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
	}
	
	public void showProduct(String date_pattern) {
		simpleDateFormat.applyPattern(date_pattern);
		showProduct();
	}
	
	public void showProduct() {
		System.out.println("id              : "+this.id);
		System.out.println("nombre          : "+this.name);
		System.out.println("precio          : "+this.price);
		System.out.println("actualizado     : "+this.simpleDateFormat.format(date_purchase));
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	
	public Date getDate_purchase() {
		return date_purchase;
	}
	
	public String getDateFormated() {
		return this.simpleDateFormat.format(date_purchase);
	}
	public void setDate_purchase(Date date_purchase) {
		this.date_purchase = date_purchase;
	}
	public void setDate_purchase(String date) {
		try {
			this.date_purchase = simpleDateFormat.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			System.out.println("No se pudo convertir la fecha");
			e.printStackTrace();
		}
	}


}
