package com.ejemplo03;

import java.util.Date;
import java.util.Scanner;

import com.ejemplo03.controller.ProductController;
import com.ejemplo03.model.Product;

public class Main {
	
	static ProductController productController;

	public static void main(String[] args) {
		// TODO Auto-generated method stu
		int opc;
		productController = new ProductController();
		Scanner scaner = new Scanner(System.in);
		do {
			opc = menu();
			switch (opc) {
			case 1:
				System.out.println("Opcion 1 - Nuevo producto");
				addProduct();
				break;
			case 2:
				System.out.println("Opcion 2 - Actualizar producto");
				updateProduct();
				break;
			case 3:
				System.out.println("Opcion 3 - Eliminar producto");
				deleteProduct();
				break;
			case 4:
				System.out.println("Opcion 4 - Listar productos");
				showProducts();
				break;
			case 5:
				System.out.println("Opcion 5 - Guardar productos");
				saveProducts();
				break;
			case 6:
				System.out.println("Opcion 6 - Cargar productos");
				loadProducts();
				break;
			case 7:
				System.out.println("Opcion 7 - Salir");
				
				break;
			default:
				System.out.println("Opcion invalida");
				break;
			}
			scaner.nextLine();
			clearConsole();
		} while (opc!=7);
		System.out.println("Ha salido de la aplicacion");
	}
	
	private static void saveProducts() {
		// TODO Auto-generated method stub
		if (productController.saveProducts("products.txt")) {
			System.out.println("Guardados");
		}else{
			System.out.println("No se pudo guardar");
		}
	}

	private static void loadProducts() {
		// TODO Auto-generated method stub
		if(productController.loadProducts("products.txt")) {
			System.out.println("Cargados");
		}else {
			System.out.println("No se pudo cargar");
		}
	}

	public static void deleteProduct() {
		// TODO Auto-generated method stub
		Scanner scaner = new Scanner(System.in);
		String id;
		do {
			System.out.println("Ingrese el id del producto : ");
			id  = scaner.nextLine();
			if(productController.findProductById(id)==null) {
				System.out.println("No se encontro Producto asociado al id\n");
			}else {
				break;
			}
		}while (true);
		
		if(productController.deleteProduct(id)) {
			System.out.println("Producto eliminado");
		}else {
			System.out.println("No se pudo eliminar el producto");
		}
		
	}

	private static Product getData(String id) {
		Scanner scaner = new Scanner(System.in);
		Product p = new Product();
		p.setId(id);
		String in;
		do {
			System.out.println("Ingrese nombre del producto : ");
			in  = scaner.nextLine();
			if(in.length()>2) {
				break;
			}else {
				System.out.println("El nombre debe ser mayor a 2 caracteres\n");
			}
		}while(true);
		p.setName(in);
		float price=-1;
		do {
			System.out.println("Ingrese el precio del producto : ");
			try {
				price = scaner.nextFloat();
				if(price>0) {
					break;
				}else {
					System.out.println("El precio debe ser mayor a cero");
				}
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("Entrada invalida \n");
				scaner.next();
			}
		}while(true);
		p.setPrice(price);
		p.setDate_purchase(new Date());
		return p;
	}

	public static void addProduct() {
		Scanner scaner = new Scanner(System.in);
		String id;
		do {
			System.out.println("Ingrese el id del producto : ");
			id  = scaner.nextLine();
			if(productController.findProductById(id)!=null) {
				System.out.println("Id se encuentra en uso, intente de nuevo\n");
			}else {
				break;
			}
		}while (true);
		
		Product p = getData(id);
		
		if(productController.addProduct(p))
			System.out.println("Producto agregado");
		else 
			System.out.println("No se pudo agregar el producto");
		
	}
	
	public static void updateProduct() {
		String id;
		Scanner scaner = new Scanner(System.in);
		do {
			System.out.println("Ingrese el id del producto : ");
			id  = scaner.nextLine();
			if(productController.findProductById(id)==null) {
				System.out.println("No se encontro Producto asociado al id\n");
			}else {
				break;
			}
		}while (true);
		Product p = getData(id);
		if (productController.updateProduct(p)) {
			System.out.println("Producto actualizado");
		}else {
			System.out.println("No se pudo actualizar el producto");
		}
	}
	
	public static void showProducts() {
		// TODO Auto-generated method stub
		for (Product p : productController.getProducts()) {
			p.showProduct();
			System.out.println();
		}
		
	}
	
	public static int menu(){
		Scanner scaner = new Scanner(System.in);
		System.out.println("Implementacion sencilla de temas del dia 1 al 4");
		System.out.println("Seleccionar una opcion valida");
		System.out.println("1.... Ingresar nuevo producto");
		System.out.println("2.... Actualizar producto");
		System.out.println("3.... Eliminar producto");
		System.out.println("4.... Listar productos");
		System.out.println("5.... Guardar productos");
		System.out.println("6.... Cargar productos");
		System.out.println("7.... Salir\n>");
		
		String text = scaner.nextLine();
		int opc = -1;
		try {
			opc = Integer.parseInt(text);
		}catch (Exception e) {
			System.out.println("Entrada no valida");
		}
		return opc;
	}
	
	public final static void clearConsole()
	{
	    try
	    {
	        final String os = System.getProperty("os.name");

	        if (os.contains("Windows"))
	        {
	            Runtime.getRuntime().exec("cls");
	        }
	        else
	        {
	            Runtime.getRuntime().exec("clear");
	        }
	    }
	    catch (final Exception e)
	    {
	        //  Handle any exceptions.
	    }
	}

}
